<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CategoriaController;
use App\Http\Controllers\PagamentoController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ProdutoController;
use App\Http\Controllers\ClienteController;
use App\Http\Controllers\PedidosController;
use App\Http\Controllers\LoginClienteController;
use Illuminate\Support\Facades\Auth;






/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

    Route::get('/home', function () {
        return view('welcome');

        
    });
    Route::group(['middleware' =>['auth','isAdmin'],'prefix'=>'home', 'where'=>['id'=>'[0-9]+']],function(){   
        Route::get('',                 ['as'=>'user.index',     'uses'=>'App\Http\Controllers\homeController@indexHome'])->middleware('auth');
    });

    Route::group(['middleware' =>['auth','isAdmin'],'prefix'=>'categorias', 'where'=>['id'=>'[0-9]+']],function(){   
        Route::get('',                 ['as'=>'categorias.index',     'uses'=>'App\Http\Controllers\CategoriaController@index'])->middleware('auth');
        Route::get('create',           ['as'=>'categorias.create',    'uses'=>'App\Http\Controllers\CategoriaController@create']);
        Route::get('{id}/destroy',     ['as'=>'categorias.destroy',   'uses'=>'App\Http\Controllers\CategoriaController@destroy']);
        Route::get('{id}/edit',        ['as'=>'categorias.edit',      'uses'=>'App\Http\Controllers\CategoriaController@edit']);
        Route::put('{id}/update',      ['as'=>'categorias.update',    'uses'=>'App\Http\Controllers\CategoriaController@update']);
        Route::post('store',           ['as'=>'categorias.store',     'uses'=>'App\Http\Controllers\CategoriaController@store']);
    });


    Route::group(['middleware' =>['auth','isAdmin'],'prefix'=>'pagamentos', 'where'=>['id'=>'[0-9]+']],function(){   
        Route::get('',                 ['as'=>'pagamentos.index',     'uses'=>'App\Http\Controllers\PagamentoController@index'])->middleware('auth');
        Route::get('create',           ['as'=>'pagamentos.create',    'uses'=>'App\Http\Controllers\PagamentoController@create']);
        Route::get('{id}/destroy',     ['as'=>'pagamentos.destroy',   'uses'=>'App\Http\Controllers\PagamentoController@destroy']);
        Route::get('{id}/edit',        ['as'=>'pagamentos.edit',      'uses'=>'App\Http\Controllers\PagamentoController@edit']);
        Route::put('{id}/update',      ['as'=>'pagamentos.update',    'uses'=>'App\Http\Controllers\PagamentoController@update']);
        Route::post('store',           ['as'=>'pagamentos.store',     'uses'=>'App\Http\Controllers\PagamentoController@store']);
    });


    Route::group(['middleware' =>['auth','isAdmin'],'prefix'=>'produtos', 'where'=>['id'=>'[0-9]+']],function(){   
        Route::any('',                 ['as'=>'produtos.index',     'uses'=>'App\Http\Controllers\ProdutoController@index'])->middleware('auth');
        Route::get('pdf',              ['as'=>'produtos.pdf',       'uses'=>"\App\Http\Controllers\ProdutoController@gerarPdf"]);
        Route::get('create',           ['as'=>'produtos.create',    'uses'=>'App\Http\Controllers\ProdutoController@create']);
        Route::get('{id}/destroy',     ['as'=>'produtos.destroy',   'uses'=>'App\Http\Controllers\ProdutoController@destroy']);
        Route::get('{id}/edit',        ['as'=>'produtos.edit',      'uses'=>'App\Http\Controllers\ProdutoController@edit']);
        Route::put('{id}/update',      ['as'=>'produtos.update',    'uses'=>'App\Http\Controllers\ProdutoController@update']);
        Route::post('store',           ['as'=>'produtos.store',     'uses'=>'App\Http\Controllers\ProdutoController@store']);
    });

    Route::group(['middleware' =>['auth','isAdmin'],'prefix'=>'pedidos', 'where'=>['id'=>'[0-9]+']],function(){ 
        Route::get('',                  ['as'=>'pedidos.index',     'uses'=>'App\Http\Controllers\PedidosController@index'])->middleware('auth');
        Route::get('/create',           ['as'=>'pedidos.create',    'uses'=>'App\Http\Controllers\PedidosController@create']);
        Route::post('/store',           ['as'=>'pedidos.store',     'uses'=>'App\Http\Controllers\PedidosController@store']);
        Route::get('/info',             ['as'=>'pedidos.info',      'uses'=>'App\Http\Controllers\PedidosController@info']);


    });

    Route::group(['prefix'=>'auth', 'where'=>['id'=>'[0-9]+']],function(){ 
        Route::post('/store',           ['as'=>'auth.store',     'uses'=>'App\Http\Controllers\UserController@store']);
    });

    Route::group(['middleware' =>['auth','isAdmin'],'prefix'=>'auth', 'where'=>['id'=>'[0-9]+']],function(){ 
        Route::any('',                  ['as'=>'auth.index',     'uses'=>'App\Http\Controllers\UserController@index']);
        Route::get('{id}/edit',         ['as'=>'auth.edit',      'uses'=>'App\Http\Controllers\UserController@edit']);
        Route::put('{id}/update',       ['as'=>'auth.update',    'uses'=>'App\Http\Controllers\UserController@update']);

    });


Auth::routes();


Route::any('/', [App\Http\Controllers\HomeController::class, 'index'])->name('shop.index');

Route::get('/carrinho', [App\Http\Controllers\CarrinhoController::class, 'carrinhoLista'])->name('carrinho.index');
Route::any('/finaliza', [App\Http\Controllers\CarrinhoController::class, 'carrinhoFinaliza'])->name('carrinho.finaliza')->middleware('auth');
Route::get('/pdfPedido', [App\Http\Controllers\CarrinhoController::class, 'gerarPdfPedido'])->name('carrinho.pdfPedido')->middleware('auth');
Route::post('/carrinho', [App\Http\Controllers\CarrinhoController::class, 'adicionaCarrinho'])->name('carrinho.addcarrinho');
Route::post('/remover', [App\Http\Controllers\CarrinhoController::class, 'removeCarrinho'])->name('carrinho.removecarrinho');
Route::post('/atualiza', [App\Http\Controllers\CarrinhoController::class, 'atualizaCarrinho'])->name('carrinho.atualizacarrinho');
Route::get('/limpar', [App\Http\Controllers\CarrinhoController::class, 'limparCarrinho'])->name('carrinho.limparcarrinho');

