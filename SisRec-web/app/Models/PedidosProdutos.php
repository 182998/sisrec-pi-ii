<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PedidosProdutos extends Model
{
    protected $table = "pedidos_produtos";
    protected $fillable = ['pedido_id', 'produto_id'];


    public function produto()
    {
        return $this->belongsTo('App\Models\Produto', 'produto_id');
    }
       public function pedidos()
    {
        return $this->belongsTo("App\Models\Pedidos",'pedido_id');
    }
    use HasFactory;
}
