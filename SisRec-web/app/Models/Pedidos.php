<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
class Pedidos extends Model
{
    protected $table = "pedido";
    protected $fillable = ['id', 'user_id', 'pagamento_id'];

    public function users()
    {
        return $this->belongsTo("App\Models\User", 'user_id');
    }
    public function pagamento()
    {
        return $this->belongsTo("App\Models\Pagamento",'pagamento_id');
    }
 
    use HasFactory;
}
