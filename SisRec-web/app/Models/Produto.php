<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Produto extends Model
{
    use HasFactory;
    protected $table = "produto";
    protected $fillable = ['nome','preco','imagem','qnt', 'ativo', 'categoria_id'];

    public function categoria()
    {
        return $this->belongsTo("App\Models\Categoria");
    }    

    public function pedidos()
    {
        return $this->hasMany('App\Models\PedidosProdutos');

    }
}
