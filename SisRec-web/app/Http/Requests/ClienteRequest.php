<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ClienteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome_cliente' => 'required',
            'cpf' => 'required',
            'cep' => 'required',
            'logradouro' => 'required',
            'numero' => 'required',
            'created_at' => 'date',
            'updated_at' => 'date'
        ];
    }
}
