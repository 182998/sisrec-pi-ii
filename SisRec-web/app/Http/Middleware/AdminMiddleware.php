<?php

namespace App\Http\Middleware;

use Closure;
Use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        if(Auth::check())
        {
            if(Auth::user()->permissao == 'admin')
            {
                return $next($request);
            }
            else
            {
                return redirect('/')->with('status', 'Acesso negado! Você não é um administrador');
            }
        }
        else
        {
            return redirect('/home')->with('status','Faça o login');

        }
    }
}
