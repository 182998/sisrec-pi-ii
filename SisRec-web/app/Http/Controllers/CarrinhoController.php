<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Pedidos;
use App\Models\PedidosProdutos;
use App\Models\Pagamento;
use PDF;

class CarrinhoController extends Controller
{
    public function carrinhoLista(){
        $itens =\Cart::getContent();
        return view('carrinho.index', compact('itens'));
    }

    public function adicionaCarrinho(Request $request)
    {
     \Cart::add([
        'id' =>$request->id,
        'name'=>$request->nome,
        'price' =>$request->preco,
        'quantity' => abs($request->qnt),
        'attributes'=> array(
            'categoria_id' => $request->categoria_id
        )
     ]);

     return redirect()->route('carrinho.index')->with('sucesso', ' Produto adicionado');
    }

    public function removeCarrinho(Request $request)
    {
        \Cart::remove($request->id);
        return redirect()->route('carrinho.index')->with('remove', ' Produto removido');
    }

    public function atualizaCarrinho(Request $request)
    {
        \Cart::update($request->id, [
            'quantity' =>[
                'relative' => false,
                'value' => abs($request->quantity),
            ],
        ]);
        return redirect()->route('carrinho.index')->with('atualiza', ' Produto atualizado');
    }

    public function limparCarrinho()
    {
        \Cart::clear();
        return redirect()->route('carrinho.index')->with('remove', 'Seu carrinho esta vazio');
    }

    public function carrinhoFinaliza(Request $request)
    {
        $itens =\Cart::getContent();
        $clientId = Auth::user()->id;
        $clientName = Auth::user()->name;
        $clientCpf = Auth::user()->cpf;
        $clientCep = Auth::user()->cep;
        $clientLogradouro = Auth::user()->logradouro;
        $clientNumero = Auth::user()->numero;
        $valorTotal = \Cart::getTotal();
        $formaPagto_id = $request->forma_pagamento;
        $formaPagto = Pagamento::find($formaPagto_id);

        $pedido = Pedidos::create([
            "user_id" => $clientId,
            "pagamento_id" => $formaPagto_id
        ]);

        foreach($itens as $produto) {
            PedidosProdutos::create([
                "pedido_id" => $pedido->id,
                "produto_id" => $produto->id
            ]);
        }
        

        return view('carrinho.finaliza', compact('itens','formaPagto','clientId','clientName','clientCpf','clientCep','clientLogradouro','clientNumero','valorTotal'));
    }
    public function gerarPdfPedido(Request $request)
    {
        $itens =\Cart::getContent();
        $clientId = Auth::user()->id;
        $clientName = Auth::user()->name;
        $clientCpf = Auth::user()->cpf;
        $clientCep = Auth::user()->cep;
        $clientLogradouro = Auth::user()->logradouro;
        $clientNumero = Auth::user()->numero;
        $valorTotal = \Cart::getTotal();
        $formaPagto = $request->forma_pagamento;
        
        $pdf = PDF::loadView('carrinho.pdfPedido', compact('itens','formaPagto','clientId','clientName','clientCpf','clientCep','clientLogradouro','clientNumero','valorTotal'));
        \Cart::clear();
        return $pdf->setPaper('a4','landscape')->stream('carrinho.pdfPedido');
        
    }
}   
