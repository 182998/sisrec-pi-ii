<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\PedidosProdutos;
use App\Models\Pedidos;
use App\Models\Produto;
use App\Models\User;

class PedidosController extends Controller
{
    public function create(){
        $produtos = Produto::all();
        return view('pedidos.create',compact('produtos'));
    }
    public function store(Request $request){
        $pedido = Pedidos::create([
            'id' => $request->get('id'),
            'user_id' =>$request->get('user_id'),
            'pagamento_id' =>$request->get('forma_pagamento'),
        ]);
        $produtos = $request->produto_id;
        foreach($produtos as $a => $value){
            PedidosProdutos::create([
                'pedido_id' => $pedido->id,
                'produto_id' => $produtos[$a]
            ]);
        }
        return redirect()->route('pedidos.index');
    }
    public function index(){
        $pedidos = PedidosProdutos::all();
        return view('pedidos.index',['pedidos'=>$pedidos]);
    }

    public function info(Request $id)
    {
        $pedidos = PedidosProdutos::find($id);


        // dd($cliente);
        return view('pedidos.info',compact('pedidos'));
    }
}
