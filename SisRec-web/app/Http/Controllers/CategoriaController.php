<?php

namespace App\Http\Controllers;

//use Illuminate\Http\Request;
use App\Models\Categoria;
use App\Http\Requests\CategoriaRequest;


class CategoriaController extends Controller
{
    public function index()
    {
        $events = Categoria::orderBy('descricao')->paginate(5);;

        return view('categorias.index',['events' => $events]);
    }

    public function create()
    {
        return view('categorias.create');
    }
    
    public function store(CategoriaRequest $request)
    {
        $nova_categoria = $request->all();
        Categoria::create($nova_categoria);
        return redirect()->route('categorias.index');
    }
    public function destroy($id)
    {
        Categoria::findOrFail($id)->delete();
        return  redirect()->route('categorias.index');
    }

    public function edit($id)
    {
        $categoria = Categoria::find($id);
        return view('categorias.edit',compact('categoria'));
    }

    public function update(CategoriaRequest $request, $id)
    {
        Categoria::find($id)->update($request->all());
        return redirect()->route('categorias.index');
    }
}
