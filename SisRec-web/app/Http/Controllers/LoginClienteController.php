<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginClienteController extends Controller
{

    public function authCliente(Request $request)
    {
        $credenciais = $request->validate([
            'email' =>['required','email'],
            'password' => ['required'],
        ]);
        if(Auth::Attempt($credenciais))
        {
            $request->session()->regenerate();
            return redirect()->intended('/loginCliente');
        }else{
            return redirect()->back()->with('error', 'emails ou senha invalidos');
        }
    }
}