<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ProdutoRequest;
use App\Models\Produto;
use PDF;


class ProdutoController extends Controller
{
    public function index(Request $filtro)
    {

        $filtragem = $filtro->get('desc_filtro');
        if ($filtragem == null)
            $events = Produto::orderBy('nome')->paginate(5);
        else
            $events = Produto::where('nome', 'like', '%'.$filtragem.'%')
                            ->orderBy("nome")
                            ->paginate(5)
                            ->setpath('produtos?desc_filtro='.$filtragem);
        return view('produtos.index', ['events'=>$events]);

        //$events = Produto::orderBy('nome')->paginate(5);
        //return view('produtos.index',['events' => $events]);
    }

    public function indexShop()
    {

       $events = Produto::orderBy('nome');
       return view('shop.index',['events' => $events]);
    }

    public function create()
    {
        return view('produtos.create');
    }

    public function store(ProdutoRequest $request)
    {
        $novo_produto = $request->all();
        $fileName = time().$request->imagem->getClientOriginalName();
        $path = $request->file('imagem')->storeAs('imagem', $fileName, 'public');
        $novo_produto['imagem'] = 'storage/'.$path;
        Produto::create($novo_produto);

        return redirect()->route('produtos.index');
    }
    public function destroy($id)
    {
        Produto::findOrFail($id)->delete();
        return redirect()->route('produtos.index');
    }

    public function edit($id)
    {
        $produto = Produto::find($id);
        // dd($cliente);
        return view('produtos.edit',compact('produto'));
    }

    public function update(ProdutoRequest $request, $id)
    {
        Produto::find($id)->update($request->all());
        $file = $request->file('imagem');
        if($file != null){
            $fileName = time().$request->imagem->getClientOriginalName();
            $path = $request->file('imagem')->storeAs('imagem', $fileName, 'public');
            $produto = Produto::find($id);
            $produto->imagem = 'storage/'.$path;
            $produto->save();
        }

        return redirect()->route('produtos.index');
    }

    public function gerarPdf()
    {
        $events = Produto::all();
        $pdf = PDF::loadView('produtos.pdf', compact('events'));
        return $pdf->setPaper('a4','landscape')->stream('todos_produtos.pdf');
    }
}
