<?php

namespace App\Http\Controllers;

use App\Models\Pagamento;
use App\Http\Requests\PagamentoRequest;

class PagamentoController extends Controller
{

    public function index()
    {
        $events = Pagamento::orderBy('forma_pagamento')->paginate(5);

        return view('pagamentos.index',['events' => $events]);
    }
    public function create()
    {
        return view('pagamentos.create');
    }

    public function store(PagamentoRequest $request)
    {
        $novo_pagamento = $request->all();
        Pagamento::create($novo_pagamento);

        return redirect()->route('pagamentos.index');
    }

    public function destroy($id)
    {
        Pagamento   ::findOrFail($id)->delete();
        return  redirect()->route('pagamentos.index');
    }

    public function edit($id)
    {
        $pagamento = Pagamento::find($id);
        return view('pagamentos.edit',compact('pagamento'));
    }

    public function update(PagamentoRequest $request, $id)
    {
        Pagamento::find($id)->update($request->all());
        return redirect()->route('pagamentos.index');
    }

}
