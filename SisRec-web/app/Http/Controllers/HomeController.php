<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\Produto;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function index()
    {
        $events = Produto::where([
            'ativo' => 'S'
        ])->get();

        return view('shop.index',compact('events'));
    }
    public function indexHome(){

        $clientName = Auth::user()->name;
        return view('user.index',compact('clientName'));

    }
}
