<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{

    public function index(Request $filtro)
    {
        $filtragem = $filtro->get('desc_filtro');
        if ($filtragem == null)
            $events = User::orderBy('name')->paginate(5);
        else
            $events = User::where('name', 'like', '%'.$filtragem.'%')
                            ->orderBy("name")
                            ->paginate(5)
                            ->setpath('auth?desc_filtro='.$filtragem);
        return view('auth.index', ['events'=>$events]);
    }


    public function edit($id)
    {
        $user = User::find($id);
        // dd($cliente);
        return view('auth.edit',compact('user'));
    }

    public function update(Request $request, $id)
    {
        User::find($id)->update($request->all());
        return redirect()->route('auth.index');
    }

    public function store(Request $request)
    {
        $novo_cliente = $request->all();
        
        $novo_cliente['password'] = bcrypt($novo_cliente['password']);
        User::create($novo_cliente);
        return redirect()->route('login');
    }

    public function create()
    {
        return view('auth.create');
    }
}

