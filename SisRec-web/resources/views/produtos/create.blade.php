@extends('adminlte::page')
@section('content')

<br>
<div class="card">
    <div class="card-header">
        <div class="col-lg-12 margin-tb" style="display: flex; justify-content: space-between;">
            <div>
                <h1>Criar Produto</h1>
            </div>
        </div>
    </div>

                     
    <div class="row mb-3">

        <div class="col-md-6">

        </div>
    </div>

@if($errors->any())
<ul class="alert alert-danger">
    @foreach($errors->all() as $error)
        <li>{{$error}}</li>
    @endforeach
</ul>
@endif
<div class="container">

    <div class="row justify-content-center">
        <div class="col-md-8">
            {!!Form::open(['route'=>'produtos.store', 'method' => 'post', 'enctype' => 'multipart/form-data'])!!}

            <div class="form-group">
                {!! Form::file('imagem', null, ['class' => 'form-control']) !!}
            </div>

            <div class="row mb-3">
                {!!Form::label('nome','Nome:')!!}
                <div class="col-md-6">
                    {!!Form::text('nome',null,['class'=>'form-controle','required'])!!}
                </div>
            </div>                                 
            <div class="row mb-3">
                {!!Form::label('preco','Preço:')!!}

                <div class="col-md-6">
                  
                    {!!Form::text('preco',null,['class'=>'form-controle','required'])!!}

                </div>
            </div>
            <div class="row mb-3">
                {!!Form::label('qnt','Quantidade:')!!}
                <div class="col-md-6">
                    {!!Form::text('qnt',null,['class'=>'form-controle','required'])!!}
                </div>
            </div>


            <div class="row mb-3">
                {!!Form::label('categoria_id','Categoria:')!!}
                <div class="col-md-6">
                    {!!Form::select('categoria_id',
                    \App\Models\Categoria::orderBy('descricao')->pluck('descricao','id')->toArray(),
                            null,['class'=>'form-control','required']) !!}
                </div>
            </div>

            <div class="row mb-3">
                {!!Form::label('ativo','Ativo:')!!}

                <div class="col-md-6">
                    {!!Form::select('ativo',array(  
                        'S'=>'Sim',
                        'N'=>'Não',),
                        'ativo',['class'=>'form-control','required'])!!}
                </div>
            </div>
 

            <div class="form-group">
                {!!Form::submit('Criar', ['class'=>'btn btn-primary'])!!}
                {!!Form::reset('Limpar',['class'=>'btn btn-primary btn-info'])!!}
                <a href="{{ route('produtos.index') }}" class="btn btn-primary btn-warning">Voltar</a>
            </div>
        </div>
    </div>
</div>
{!!Form::close()!!}

@stop