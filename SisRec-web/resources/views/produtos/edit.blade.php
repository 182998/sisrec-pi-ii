@extends('adminlte::page')
@section('content')
<br>


<div class="card">
    <div class="card-header">
        <div class="col-lg-12 margin-tb" style="display: flex; justify-content: space-between;">
            <div>
                <h1>Criar Produto: {{$produto->nome}}</h1>
            </div>
        </div>
    </div>

@if($errors->any())
<ul class="alert alert-danger">
    @foreach($errors->all() as $error)
        <li>{{$error}}</li>
    @endforeach
</ul>
@endif
<div class="card-body">
    <div class="row">
    {!!Form::open(['route'=>["produtos.update",'id'=>$produto->id],'method'=>'put','enctype' => 'multipart/form-data'])!!}

        <div class="form-group">
            <input id="attachment" type="file" class="form-control-file" name="imagem" value="{{ old('imagem')}}">
        </div>

        <div class="form-group">
            {!!Form::label('nome','Nome:')!!}
            {!!Form::text('nome',$produto->nome,['class'=>'form-controle','required'])!!}
        </div>
    
        <div class="form-group">
            {!!Form::label('preco','Preço:')!!}
            {!!Form::text('preco',$produto->preco,['class'=>'form-controle','required'])!!}
        </div>

        <div class="form-group">
            {!!Form::label('qnt','Quantidade:')!!}
            {!!Form::text('qnt',$produto->qnt,['class'=>'form-controle','required'])!!}
        </div>

        <div class="form-group">
            {!!Form::label('categoria_id','Categoria:')!!}
            {!!Form::select('categoria_id',
                            \App\Models\Categoria::orderBy('descricao')->pluck('descricao','id')->toArray(),
                                    $produto->categoria_id,['class'=>'form-control','required']) !!}
        </div>
        <div class="form-group">
            {!!Form::label('ativo','Ativo:')!!}
            {!!Form::select('ativo',array(  
                                    'S'=>'Sim',
                                    'N'=>'Não',),
                                    ['class'=>'form-control','required'])
            !!}
        </div>

        <div class="form-group">
            {!!Form::submit('Editar Produto', ['class'=>'btn btn-primary btn-success'])!!}
            <a href="{{ route('produtos.index') }}" class="btn btn-primary btn-warning">Voltar</a>
        </div>
    </div>
</div>
{!!Form::close()!!}


@stop