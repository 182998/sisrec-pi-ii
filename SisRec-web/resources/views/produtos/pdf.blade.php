<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Produtos</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    </head>
    <body>
    <div>
        <h1>Produtos</h1>
        <table class="table table-bordered mb-6">


            <thead>
            <tr class="table-info">
                <th scope="col"></th>
                <th scope="col">ID</th>
                <th scope="col">Produto</th>
                <th scope="col">Preço</th>
                <th scope="col">Quantidade</th>
                <th scope="col">Ativo</th>
                <th scope="col">Categoria</th>

            </tr>
            </thead>

            @foreach($events as $event)
                <tr class="table-hover">
                    @php($p = \App\Models\Produto::whereId($event->id)->first())
                    <td><p id="imagem">{!! $p->imagem ? "<img width=\"100\" src=\"$p->imagem\">" : 'Sem Foto' !!}</p></td>
                    <td>{{$event->id}}</td>
                    <td>{{$event->nome}}</td>
                    <td>R$ {{number_format($event->preco,2,',','.')}} </td>
                    <td>{{$event->qnt}}</td>
                    <td>{{$event->ativo}}</td>
                    <td>{{$event->categoria->descricao}}</td>
                </tr>
            @endforeach
        </table>
    </div>
    <script src="{{ asset('js/app.js') }}" type="text/js"></script>
    </body>
</html>
