@extends('layouts.default')
@section('content')
<br>
<div class="card">
    <div class="card-header">
        <div class="col-lg-12 margin-tb" style="display: flex; justify-content: space-between;">
            <div>
                <h1>Lista de produtos</h1>
            </div>
            <div>
                <a href="{{ route('produtos.create') }}" class=" btn btn-primary btn-sm btn-success">Adicionar</a>
                <a href="{{ route('produtos.pdf') }}" class="btn btn-primary btn-sm btn-success">Gerar PDF</a>

            </div>
        </div>
    </div>
    {!! Form::open(['name'=>'form_name', 'route'=>'produtos.index']) !!}
    <div calss="sidebar-form">
        <div class="input-group">
            <input type="text" name="desc_filtro" class="form-control" style="width:80% !important;" placeholder="Pesquisa...">
            <span class="input-group-btn">
                	<button type="submit" name="search" id="search-btn" class="btn btn-default"><i class="fa fa-search"></i></button>
              	</span>
        </div>
    </div>


    {!! Form::close() !!}
    <div class="card-body">
        <table class="table table-strip table-bordered table-hover">
            <thead>
                <th>Imagem</th>
                <th>Codigo</th>
                <th>Nome</th>
                <th>Preço</th>
                <th>Quantidade</th>
                <th>Ativo</th>
                <th>Categoria</th>

            </thead>
            <tbody>
                @foreach ($events as $event)
                        <tr>
                            <td id="imagem">{!! $event->imagem ? "<img width=\"61\" src=\"$event->imagem\">" : 'Sem Foto' !!}</td>
                            <td>{{$event->id}}</td>
                            <td>{{$event->nome}}</td>
                            <td>R$ {{number_format($event->preco,2,',','.')}} </td>
                            <td>{{$event->qnt}}</td>
                            <td>{{$event->ativo}}</td>
                            @if($event->categoria)
                            <td>{{$event->categoria->descricao}}</td>
                            @else
                            <td>-</td>
                            @endif
                            <td>
                                <div class="col-lg-12 margin-tb" style="display: flex; justify-content: space-between;">
                                    <div class="row">
            
                                        <div>
                                            <a href="{{ route('produtos.edit', ['id'=>$event->id]) }}" class="btn  btn-outline-warning btn-sm">Editar</a>
                                        </div>
                                        <div>
                                            <a href="#" onclick="return ConfirmaExclusao({{$event->id}})" class="btn  btn-outline-danger btn-sm">Remover</a>
                                        </div>
                                    </div>
                                 </div>
                            </td>
                        </tr>
                    @endforeach
            </tbody>
        </table>
    </div>
</div>
{{$events->links("pagination::bootstrap-4")}}

@stop
@section('table-delete')
"produtos"
@endsection
