@extends('adminlte::page')

@section('content')
<br>
    <div class="card">
        <div class="card-header" style="background: lightgrey">
            <h1>Novo pedido</h1>
        </div>
        <div class="card-body">
            {!!Form::open(['route'=>'pedidos.store'])!!}
                <div class="form-group">
                    {!!Form::label('user_id','Cliente:')!!}
                    {!!Form::select('user_id',
                                    \App\Models\User::orderBy('name')->pluck('name','id')->toArray(),
                                            null,['class'=>'form-control','required']) !!}
                </div>

                <div class="form-group">
                    {!!Form::label('forma_pagamento','Forma pagamento:')!!}
                    {!!Form::select('forma_pagamento',
                                    \App\Models\Pagamento::orderBy('forma_pagamento')->pluck('forma_pagamento','id')->toArray(),
                                            null,['class'=>'form-control','required']) !!}
                </div>
                <hr/>
                <h4>Produtos</h4>
                <div class="input_fields_wrap"></div>
                <br>
                <button style="float:right" class="add_field_button btn btn-default"> Adicionar Produto</button>
                <br>
                <hr/>
                <div class="form_group">
                {!!Form::submit('Criar pedido',['class'=>'btn btn-primary'])!!}
            </div>
            {!!Form::close()!!}
        </div>
    </div>
@stop

@section('js')
    <script>
        $(document).ready(function(){
            var wrapper = $(".input_fields_wrap");
            var add_button = $(".add_field_button");
            var x=0;
            $(add_button).click(function(e){
                x++;
                var newField = '<div><div style="width:94%; float:left" id="produto">{!!Form::select("produto_id[]",\App\Models\Produto::orderBy("nome")->pluck("nome","id")->toArray(),null,["class"=>"form-control","required","placeholder"=>"Selecione um produto"])!!}</div><button type="button" class="remove_field btn btn-danger btn-circle"><i class="fa fa-times"></button></div>';
                    $(wrapper).append(newField);
                });
                $(wrapper).on("click",".remove_field",function(e){
                    e.preventDefault();
                    $(this).parent('div').remove();
                    x--;
                });
        })
        </script>
        @stop