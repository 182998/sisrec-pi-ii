@extends('layouts.default')
@section('content')
<div class="container-fluid">
    
    <h3> Pedidos</h3>
    <table class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th>Pedido</th>
                <th>Cliente</th>
                <th>CPF</th>
                <th>CEP</th>
                <th>Logradouro</th>
                <th>Numero</th>
                <th>Forma de pagamento</th>
            </tr>
        </thead>
            <tbody>
                @foreach ($pedidos as $pedido)
                <tr>
                    <td> {{$pedido->id}}</td>
                    <td> {{$pedido->pedidos->users->name}}</td>
                    <td> {{$pedido->pedidos->users->cpf}}</td>
                    <td> {{$pedido->pedidos->users->cep}}</td>
                    <td> {{$pedido->pedidos->users->logradouro}}</td>
                    <td> {{$pedido->pedidos->users->numero}}</td>

                    <td> {{$pedido->pedidos->pagamento->forma_pagamento}}</td>
                </tr>
            @endforeach
    </table>
    <table class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th>Produtos</th>
                <th>Preço</th>
            </tr>
        </thead>
    </tbody>
        @foreach ($pedidos as $pedido )
        <tr>
            <td> {{$pedido->produto->nome}}</td>
            <td>R$ {{number_format($pedido->produto->preco,2,',','.')}}</td>
        </tr>
        @endforeach
     </table>
    </div>
</div>
@stop
@section('table-delete')
"pedidos"
@endsection
