@extends('layouts.default')
@section('content')
<br>
<div class="container-fluid">
    
    <h3> Pedidos</h3>
    </table>
    <a href="{{route('pedidos.create',[])}}" class="btn btn-info">Adicionar</a>
    </div>
    <br>
    <table class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th>Pedido</th>
                <th>Cliente</th>
                <th>Data de compra</th>

                <th></th>
            </tr>
        </thead>
            <tbody>
                @foreach ($pedidos as $pedido)
                <tr>
                    <td> {{$pedido->id}}</td>
                    <td> {{$pedido->pedidos->users->name}}</td>
                    <td> {{$pedido->created_at}}</td>
                    
                    <td>
                        <a href="{{ route('pedidos.info', ['id'=>$pedido->id]) }}" class="btn  btn-outline-warning btn-sm">Detalhes</a>
                    </td>
            @endforeach
            </tbody>

</div>
@stop
@section('table-delete')
"pedidos"
@endsection
