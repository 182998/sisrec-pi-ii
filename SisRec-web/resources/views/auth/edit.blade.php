@extends('adminlte::page')
@section('content')



<div class="card">
    <div class="card-header">
        <div class="col-lg-12 margin-tb" style="display: flex; justify-content: space-between;">
            <div>
                <h1>Editando Cliente: {{$user->name}} </h1>
            </div>
        </div>
    </div>

@if($errors->any())
<ul class="alert alert-danger">
    @foreach($errors->all() as $error)
        <li>{{$error}}</li>
    @endforeach
</ul>
@endif
<div class="card-body">
    <div class="row">
    {!!Form::open(['route'=>["auth.update",'id'=>$user->id], 'method'=>'put'])!!}


        <div class="form-group">
            {!!Form::label('permissao','Permissão:')!!}
            {!!Form::select('permissao',array(  
                                    'cliente'=>'Cliente',
                                    'admin'=>'Admin',),
                                    ['class'=>'form-control','required'])
            !!}
        </div>

        <div class="form-group">
            {!!Form::label('name','Nome:')!!}
            {!!Form::text('name',$user->name,['class'=>'form-controle','required'])!!}
        </div>

        
        <div class="form-group">
            
            {!!Form::label('cpf','CPF:')!!}
            {!!Form::text('name',$user->cpf,['class'=>'form-controle','required','disabled'=>'true'])!!}

        </div>
        
        <div class="form-group">
            {!!Form::label('email','Email:')!!}
            {!!Form::text('name',$user->email,['class'=>'form-controle','required','disabled'=>'true'])!!}
        </div>

        <div class="form-group">
            {!!Form::label('cep','CEP:')!!}
            {!!Form::text('cep',$user->cep,['class'=>'form-controle','required'])!!}
        </div>

        <div class="form-group">
            {!!Form::label('logradouro','Logradouro:')!!}
            {!!Form::text('logradouro',$user->logradouro,['class'=>'form-controle','required'])!!}
        </div>

        <div class="form-group">
            {!!Form::label('numero','Numero:')!!}
            {!!Form::text('numero',$user->numero,['class'=>'form-controle','required'])!!}
        </div>

        <div class="form-group">
            {!!Form::submit('Editar', ['class'=>'btn btn-primary btn-success'])!!}
            <a href="{{ route('auth.index') }}" class="btn btn-primary btn-warning">Voltar</a>
    </div>
</div>
    {!!Form::close()!!}
@stop