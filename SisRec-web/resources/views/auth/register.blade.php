@extends('layouts.app')
@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    {!!Form::open(['route'=>'auth.store', 'method' => 'post'])!!}
                    
                    <div class="row mb-3">
                        {!!Form::label('name','Nome:',['class'=>'col-md-4 col-form-label text-md-end'])!!}
                        <div class="col-md-6">
                            {!!Form::text('name',null,['class'=>'form-controle','required', 'placeholder'=>'nome completo'])!!}
                        </div>
                    </div>

                    <div class="row mb-3">
                        {!!Form::label('email','Email:',['class'=>'col-md-4 col-form-label text-md-end'])!!}
                        <div class="col-md-6">
                            {!!Form::text('email',null,['class'=>'form-controle','required', 'placeholder'=>'exemplo@exemplo'])!!}
                        </div>
                    </div>


                    <div class="row mb-3">
                        {!!Form::label('password','Password:',['class'=>'col-md-4 col-form-label text-md-end'])!!}
                        <div class="col-md-6">
                            {!!Form::password('password',null,['class'=>'form-controle','required'])!!}
                        </div>
                    </div>
                        
                        <div class="row mb-3">
                            {!!Form::label('cpf','CPF:',['class'=>'col-md-4 col-form-label text-md-end'])!!}
                            <div class="col-md-6">
                                {!!Form::text('cpf',null,['class'=>'form-controle','required','maxlength'=>"11", 'data-mask'=>'000.000.000-00','placeholder'=>'000.000.000-00'])!!}
                            </div>
                        </div>
                                                
                        <div class="row mb-3">
                            {!!Form::label('cep','CEP:',['class'=>'col-md-4 col-form-label text-md-end'])!!}
                            <div class="col-md-6">
                                {!!Form::text('cep',null,['class'=>'form-controle cep-mask','required','maxlength'=>"8", 'placeholder'=>'00000-000'])!!}
                            </div>
                        </div>

                        <div class="row mb-3">
                            {!!Form::label('logradouro','Logradouro:',['class'=>'col-md-4 col-form-label text-md-end'])!!}
                            <div class="col-md-6">
                                {!!Form::text('logradouro',null,['class'=>'form-controle','required','maxlength'=>"8", 'placeholder'=>'rua...'])!!}
                            </div>
                        </div>

                
                        <div class="row mb-3">
                            {!!Form::label('numero','Numero:',['class'=>'col-md-4 col-form-label text-md-end'])!!}
                            <div class="col-md-6">
                                {!!Form::text('numero',null,['class'=>'form-controle','required'])!!}
                            </div>
                        </div>

                        <div class="row mb-0">
                            <div class="col-md-6 offset-md-4">
                                {!!Form::submit('Registrar',['class'=>'btn btn-primary btn-info'])!!}

                            </div>
                        </div>
                    {!!Form::close()!!}
                </div>
            </div>
        </div>
    </div>
</div>
@stop
