@extends('layouts.default')
@section('content')

<br>
<div class="card">
    <div class="card-header">
        <div class="col-lg-12 margin-tb" style="display: flex; justify-content: space-between;">
            <div>
                <h1>Lista de clientes</h1>
            </div>

        </div>
    </div>

    {!! Form::open(['name'=>'name', 'route'=>'auth.index']) !!}
    <div calss="sidebar-form">
        <div class="input-group">
            <input type="text" name="desc_filtro" class="form-control" style="width:80% !important;" placeholder="Pesquisa...">
            <span class="input-group-btn">
                	<button type="submit" name="search" id="search-btn" class="btn btn-default"><i class="fa fa-search"></i></button>
              	</span>
        </div>
    </div>


    {!! Form::close() !!}

    <br>


    <div class="card-body">
    <table class="table table-strip table-bordered table-hover">
        <thead>
            <th>Codigo</th>
            <th>Email</th>
            <th>Permissão</th>
            <th>Nome</th>
            <th>CPF</th>
            <th>CEP</th>
            <th>Logradouro</th>
            <th>numero</th>
        </thead>
        <tbody>
            @foreach ($events as $event)
                    <tr>
                        <td>{{$event->id}}</td>
                        <td>{{$event->email}}</td>
                        <td>{{$event->permissao}}</td>
                        <td>{{$event->name}}</td>
                        <td>{{$event->cpf}}</td>
                        <td>{{$event->cep}}</td>
                        <td>{{$event->logradouro}}</td>
                        <td>{{$event->numero}}</td>
                        <td>
                            <div class="col-lg-12 margin-tb" style="display: flex; justify-content: space-between;">
                                <div class="row">
                                    <div>
                                        <a href="{{ route('auth.edit', ['id'=>$event->id]) }}"class="btn  btn-outline-warning btn-sm">Editar</a>
                                    </div>
                                </div>
                            </div>
                        </td>

                    </tr>
                @endforeach
        </tbody>
    </table>
    </div>
</div>
@stop
@section('table-delete')
"users"
@endsection
