@extends('adminlte::page')
@section('content')

<br>

<div class="card">
    <div class="card-header">
        <div class="col-lg-12 margin-tb" style="display: flex; justify-content: space-between;">
            <div>
                <h1>Aditar fomra de pagamento: {{$pagamento->forma_pagamento}}</h1>
            </div>
        </div>
    </div>

@if($errors->any())
<ul class="alert alert-danger">
    @foreach($errors->all() as $error)
        <li>{{$error}}</li>
    @endforeach
</ul>
@endif

<div class="card-body">
    <div class="row">
    {!!Form::open(['route'=>["pagamentos.update",'id'=>$pagamento->id], 'method'=>'put'])!!}
        <div class="form-group">
            {!!Form::label('nome','Pagamento:')!!}
            {!!Form::text('forma_pagamento',$pagamento->forma_pagamento,['class'=>'form-controle','required'])!!}
        </div>

        <div class="form-group">
            {!!Form::submit('Editar Pagamento', ['class'=>'btn btn-primary btn-success'])!!}
            <a href="{{ route('pagamentos.index') }}" class="btn btn-primary btn-warning">Voltar</a>
        </div>
    </div>
</div>
{!!Form::close()!!}
@stop