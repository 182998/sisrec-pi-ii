@extends('adminlte::page')
@section('content')
<br>
<div class="card">
    <div class="card-header">
        <div class="col-lg-12 margin-tb" style="display: flex; justify-content: space-between;">
            <div>
                <h1>Criar forma de pagamento</h1>
            </div>
        </div>
    </div>

@if($errors->any())
<ul class="alert alert-danger">
    @foreach($errors->all() as $error)
        <li>{{$error}}</li>
    @endforeach
</ul>
@endif
<div class="card-body">
    <div class="row">
        {!!Form::open(['route'=>'pagamentos.store'])!!}
            <div class="form-group">
                {!!Form::label('nome','Pagamento:')!!}
                {!!Form::text('forma_pagamento',null,['class'=>'form-controle','required'])!!}
            </div>
            <!--
            <div class="form-group">
                {!!Form::label('updated_at', 'Data de Nascimento:')!!}
                {!!Form::date('updated_at',null,['class'=>'form-control', 'required'])!!}
            </div>

            <div class="form-group">
                {!!Form::label('created_at', 'Inicio atividades:')!!}
                {!!Form::date('created_at',null,['class'=>'form-control', 'required'])!!}
            </div>
        -->
            <div class="form-group">
                {!!Form::submit('Criar', ['class'=>'btn btn-primary'])!!}
                {!!Form::reset('Limpar',['class'=>'btn btn-primary btn-info'])!!}
                <a href="{{ route('pagamentos.index') }}" class="btn btn-primary btn-warning">Voltar</a>
            </div>
    </div>
</div>

{!!Form::close()!!}
@stop