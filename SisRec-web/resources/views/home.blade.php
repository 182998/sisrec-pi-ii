@extends('adminlte::page')
@section('content')
    @if($mensagem = Session::get('status'))
    <div class="card bg-success">
        <div class="card-header">
            <h3 class="card-title">Success</h3>
            <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i>
                    </button>
                    </div>
                </div>
                <div class="card-body">
                    <p>{{$mensagem}}</p>
            </div>
        </div>
    @endif

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>
               
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('You are logged in!') }}
                </div>
            </div>
            
            <div class="col-lg-3 col-6">

                <div class="small-box bg-info">
                    <div class="inner">
                        <h3>150</h3>
                        <p>New Orders</p>
                            </div>
                            <div class="icon">
                            <i class="fas fa-shopping-cart"></i>
                            </div>
                        <a href="#" class="small-box-footer">
                        More info <i class="fas fa-arrow-circle-right"></i>
                        </a>
                    </div>
                </div>  
        </div>
    </div>
</div>
@stop
