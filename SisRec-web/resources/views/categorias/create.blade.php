@extends('adminlte::page')
@section('content')

<br>
    
    <div class="card">
        <div class="card-header">
            <div class="col-lg-12 margin-tb" style="display: flex; justify-content: space-between;">
                <div>
                    <h1> Cria uma Categoria</h1>
                </div>
            </div>
        </div>
    
    @if($errors->any())
        <ul class="alert alert-danger">
            @foreach($errors->all() as $error)
                <li>{{$error}}</li>
            @endforeach
        </ul>
    @endif
    <div class="card-body">
        <div class="row">
        {!!Form::open(['route'=>'categorias.store'])!!}
            <div class="form-group">
                {!!Form::label('nome','Categoria:')!!}
                {!!Form::text('descricao',null,['class'=>'form-controle','required'])!!}
            </div>

            <!-- 
        <div class="form-group">
                {!!Form::label('nacionalidade','Nacionalidade:')!!}
                {!!Form::select('nacionalidade',
                                array(  'BRA'=>'Brasileiro',
                                        'USA'=>'Americano',
                                        'CAN'=>'Canadense',
                                        'ARG'=>'Atgentino'),
                                        'BRA',['class'=>'form-control','required'])
                                        !!}
            </div>
        -->
            <div class="form-group">
                {!!Form::submit('Criar', ['class'=>'btn btn-primary'])!!}
                {!!Form::reset('Limpar',['class'=>'btn btn-primary btn-info'])!!}
                <a href="{{ route('categorias.index') }}" class="btn btn-primary btn-warning">Voltar</a>
            </div>
        </div>
    </div>
</div>

    {!!Form::close()!!}
@stop