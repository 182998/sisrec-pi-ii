@extends('layouts.default')
@section('content')
<br>

    <div class="card">
        <div class="card-header">
            <div class="col-lg-12 margin-tb" style="display: flex; justify-content: space-between;">
                <div>
                    <h1>Categorias</h1>
                </div>
                <div>
                    <a href="{{ route('categorias.create') }}" class="btn btn-primary btn-sm btn-success">Adicionar</a>
                </div>
            </div>
        </div>

        <div class="card-body">
            <table class="table table-strip table-bordered table-hover">
                <thead>
                    <th>Codigo</th>
                    <th>Categoria</th>
                </thead>
                <tbody>
                    @foreach ($events as $event)
                            <tr>
                                <td>{{$event->id}}</td>
                                <td>{{$event->descricao}}</td>
                                <td>
                                    <div class="col-lg-12 margin-tb" style="display: flex; justify-content: space-between;">
                                        <div class="row">
                                            <div>
                                                <a href="{{ route('categorias.edit', ['id'=>$event->id]) }}" class="btn  btn-outline-warning btn-sm">Editar</a>
                                            </div>
                                            <div>
                                                <a href="#" onclick="return ConfirmaExclusao({{$event->id}})"class="btn  btn-outline-danger btn-sm">Remover</a>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                </tbody>
            </table>
        </div>
    </div>

        {{$events->links("pagination::bootstrap-4")}}
        
        @stop
        @section('table-delete')
        "categorias"
        @endsection