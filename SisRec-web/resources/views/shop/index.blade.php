@extends('layouts.cliente')
@section('content')
    @if($mensagem = Session::get('status'))
    <div class="card bg-warning">
        <div class="card-header">
            <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i>
                    </button>
                    </div>
                </div>
                <div class="card-body">
                    <p>{{$mensagem}}</p>
            </div>
        </div>
    @endif
        <tbody>
            <div class="container" id="container-carrinho">
                <div class="row">
                        @foreach ($events as $event)
                        <div class="col">
                            <br>
                        <div class="card text-center" style="width:300px;">
                            <p id="imagem">{!! $event->imagem ? "<img width=\"300\" src=\"$event->imagem\">" : 'Sem Foto' !!}</p>
                            <div class="card-body text-center" >
                                <p class="card-nome"><h5>{{$event->nome}}</h5></p>
                                <p class="card-preco"><h2>R$ {{number_format($event->preco,2,',','.')}}</h2>  </p>
                                <p class="card-nome"><h5> {{$event->categoria->descricao}}</h5></p>
                                <!--Botão de comprar-->
                                <p class="card-button">
                                <form action="{{route('carrinho.addcarrinho')}}" method="POST" enctype="multipart/form-data"> 
                                @csrf
                                <input type="hidden" name="id" value="{{$event->id}}">
                                <input type="hidden" name="nome" value="{{$event->nome}}">
                                <input type="hidden" name="preco" value="{{$event->preco}}">
                                <input type="hidden" min="1" name="qnt" value="{{$event->qnt}}">
                                <input type="hidden" name="categoria_id" value="{{$event->categoria->descricao}}">
                                <input type="hidden" name="imagem" value="{{$event->imagem}}">    
                                <button class="btn  btn-outline-success btn-sm"> Comprar</button> 
                                </form>
                                </p>
                            </div>
                          </div>
                        </div>
                        @endforeach
                </div>
            </div>
        </tbody>
@endsection
