@extends('layouts.cliente')
@section('title', 'carrinho de compras')
@section('content')

<div class="container" style="display: flex;
                            flex-direction: row;
                            justify-content: center;
                            align-items: center;">
    <div class="box">
        <div class="card" style="width:600px;   text-align: center; justify-content: center;">
            <div class="card">
                <div class="card-header">
                    <h3>Dados do Cliente</h3>
                    <br>
                        <div class="row">
                            <div class="col-sm-4">
                                <p>Cliente: {{$clientName}}</p>
                            </div>
                            <div class="col-sm-4">
                                <p>CPF: {{$clientCpf}} </p>
                            </div>
                            <div class="col-sm-4">
                                <p>CEP: {{$clientCep}}</p>
                            </div>
                            <div class="col-sm-4">
                                <p>Endereço: {{$clientLogradouro}}</p>
                            </div>
                            <div class="col-sm-4">
                                <p>Numero: {{$clientNumero}}</p>
                            </div>
                        </div>
                </div>
                <div class="card-body">
                    <table class="table table-strip table-bordered table-hover">
                        <thead>
                            <th></th>
                            <th>Nome</th>
                            <th>Preço</th>
                            <th>Categoria</th>
                            <th>Quantidade</th>
                        </thead>
                        <tbody>
                            <h3>Itens do Pedido</h3>
                            <div>
                            @foreach ($itens as $item)
                                <div>
                                    <tr>
                                        @php($p = \App\Models\Produto::whereId($item->id)->first())
                                        <td><p id="imagem">{!! $p->imagem ? "<img width=\"50\" src=\"$p->imagem\">" : 'Sem Foto' !!}</p>
                                        </td>
                                        <td>{{$item->name}}</td>
                                        <td>R$ {{number_format($item->price,2,',','.')}} </td>
                                        <td>{{$item->attributes->categoria_id}}</td>
                                        <td>{{$item->quantity}}</td>
                                    </tr>
                                </div>
                            @endforeach
                            </div>
                        </tbody>
                    </table> 
                </div>
                <div class="card-footer">
                    <div class="col-sm-4">
                        <p>Pagamento:{{$formaPagto['forma_pagamento']}} </p>
                    </div>
                    <div class="col-sm-4">
                        <p>Valor total: R${{number_format($valorTotal,2,',','.')}} </p>
                    </div>
                    <div class="col-sm-4">
                        <a href="{{ route('carrinho.pdfPedido') }}" class="btn btn-outline-primary btn-lg">Imprimir</a>
                    </div>
                </div>
            </div>   
        </div>
    </div>
</div>
@stop
@section('table-delete')
"finaliza"
@endsection