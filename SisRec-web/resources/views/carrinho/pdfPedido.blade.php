<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    </head>
    <body>

        <div class="container" style="display: flex;
                                    flex-direction: row;
                                    justify-content: center;
                                    align-items: center;">
            <div class="card-header">
                <h3>Detalhes do Pedido</h3>
                    <div class="row">
                        <div class="col-sm-4">
                            Cliente: {{$clientName}}
                        </div>
                        <div class="col-sm-4">
                            CPF: {{$clientCpf}}
                        </div>
                        <div class="col-sm-4">
                            CEP: {{$clientCep}}
                        </div>
                        <div class="col-sm-4">
                            Endereço: {{$clientLogradouro}}
                        </div>
                        <div class="col-sm-4">
                            Numero: {{$clientNumero}}
                        </div>
                    </div>
               </div>
               <div class="card-body">
                <table class="table table-bordered mb-6">
                    <thead>
                        <tr class="table-info">
                            <th scope="col"></th>
                            <th scope="col">Nome</th>
                            <th scope="col">Produto</th>
                            <th scope="col">Categoria</th>
                            <th scope="col">Quantidade</th>
                        </tr>
                    </thead>
                        @foreach ($itens as $item)
                                <tr>
                                    @php($p = \App\Models\Produto::whereId($item->id)->first())
                                    <td><p id="imagem">{!! $p->imagem ? "<img width=\"50\" src=\"$p->imagem\">" : 'Sem Foto' !!}</p>
                                    </td>
                                    <td>{{$item->name}}</td>
                                    <td>R$ {{number_format($item->price,2,',','.')}} </td>
                                    <td>{{$item->attributes->categoria_id}}</td>
                                    <td>{{$item->quantity}}</td>
                                </tr>
                        @endforeach
                </table>
               </div>
                        <div class="card-footer">
                            <div>
                                <p> Valor total: R${{number_format($valorTotal,2,',','.')}}</p>
                            </div>
        </div>     
    </body>
</html>
