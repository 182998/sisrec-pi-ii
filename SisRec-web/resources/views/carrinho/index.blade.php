@extends('layouts.cliente')
@section('title', 'carrinho de compras')
@section('content')

<div class="card">
    @if($mensagem = Session::get('sucesso'))
    <div class="card bg-success">
        <div class="card-header">
            <h3 class="card-title">Success</h3>
            <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i>
                    </button>
                    </div>
                </div>
                <div class="card-body">
                    <p>{{$mensagem}}</p>
            </div>
        </div>
    @endif

    @if($mensagem = Session::get('remove'))
    <div class="card bg-warning">
        <div class="card-header">
            <h3 class="card-title">Success</h3>
            <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i>
                    </button>
                    </div>
                </div>
                <div class="card-body">
                    <p>{{$mensagem}}</p>
            </div>
        </div>
    @endif

    @if($mensagem = Session::get('atualiza'))
    <div class="card bg-primary">
        <div class="card-header">
            <h3 class="card-title">Success</h3>
            <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i>
                    </button>
                    </div>
                </div>
                <div class="card-body">
                    <p>{{$mensagem}}</p>
            </div>
        </div>


    @endif

    <div class="card-header">
        <div style="display: flex; justify-content: space-between;">
        </div>
    </div>
    <div class="card-body">
        <table class="table table-strip table-bordered table-hover">
            <thead>
                <th></th>
                <th>Nome</th>
                <th>Preço</th>
                <th>Categoria</th>
                <th>Quantidade</th>
            </thead>
            <tbody>
                <div>
                  <?php $i =0;?>
                @foreach ($itens as $item)
                    <div>
                        <tr>
                            @php($p = \App\Models\Produto::whereId($item->id)->first())
                            <td><p id="imagem">{!! $p->imagem ? "<img width=\"100\" src=\"$p->imagem\">" : 'Sem Foto' !!}</p>
                            </td>
                            <td>{{$item->name}}</td>
                            <td>R$ {{number_format($item->price,2,',','.')}} </td>
                            <td>{{$item->attributes->categoria_id}}</td>
                                {{-- BTN Atualiza --}}
                                <form action="{{route('carrinho.atualizacarrinho')}}" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    <input type="hidden" name="id" value="{{$item->id}}">
                                    <td><input style="width: 40px; font-weight 900;" class="white center" min="1" max="{{$item->quantity}}" type="number" name="quantity" value="{{$item->quantity}}"></td>
                                    <td>
                                    <button class="btn  btn-outline-primary btn-sm">Atualizar</button>
                                </form>
                                {{-- BTN remove --}}
                                <form action="{{route('carrinho.removecarrinho')}}" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    <input type="hidden" name="id" value="{{$item->id}}">
                                    <button class="btn  btn-outline-danger btn-sm">Deleta</button>
                                </form>

                                </td>
                            </td>
                        </tr>
                    </div>
                @endforeach
                </div>
            </tbody>
        </table>    
        <div class="row">
            <div class="col-6">
                <div class="card" >
                    <div class="card-body">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title"></h3>
                                <div class="card-tools">
                                    <h5>Valor total: R$ {{number_format(\Cart::getTotal(),2,',','.')}} </h5>

                                </div>
                            </div>
                        </div>
                        <a href="{{ route('shop.index') }}" class="btn btn-outline-primary btn-lg">Continuar comprando</a>
                        <a href="{{ route('carrinho.limparcarrinho') }}" class="btn btn-outline-warning btn-lg">Limpar carrinho</a>
                        {!!Form::open(['route'=>["carrinho.finaliza",'id'=>$item->id],'method'=>'put','enctype' => 'multipart/form-data'])!!}

                        {!!Form::label('forma_pagamento','Forma pagamento:')!!}
                                        {!!Form::select('forma_pagamento',
                                                        \App\Models\Pagamento::orderBy('forma_pagamento')->pluck('forma_pagamento','id')->toArray(),
                                                                null,['class'=>'form-control','required']) !!}
                        {!!Form::submit('Finalizar pedido', ['class'=>'btn btn-primary btn-success'])!!}
                        {!!Form::close()!!}
                    </div>
                </div>    
            </div>
        </div>
    </div>
</div>
@stop
@section('table-delete')
"carrinho"
@endsection
