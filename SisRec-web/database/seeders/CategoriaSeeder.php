<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Categoria;

class CategoriaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Categoria::create(['descricao' => 'MOUSE']);
        Categoria::create(['descricao' => 'TECLADO']);
        Categoria::create(['descricao' => 'PERIFERICOS']);
        Categoria::create(['descricao' => 'CASA']);
        Categoria::create(['descricao' => 'COMPUTADOR']);
        Categoria::create(['descricao' => 'GAMER']);
        Categoria::create(['descricao' => 'FONE']);
        Categoria::create(['descricao' => 'HEADSET']);
        Categoria::create(['descricao' => 'MOUSE PAD']);
        Categoria::create(['descricao' => 'CABOS E ADAPTADORES']);
        Categoria::create(['descricao' => 'CAIXA DE SOM']);
        Categoria::create(['descricao' => 'FONE DE OUVIDO GAMER']);
    }

}
