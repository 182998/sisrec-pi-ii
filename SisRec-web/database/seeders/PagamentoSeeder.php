<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use  App\Models\Pagamento;
class PagamentoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Pagamento::create(['forma_pagamento' => 'PIX']);
        Pagamento::create(['forma_pagamento' => 'DÉBITO']);
        Pagamento::create(['forma_pagamento' => 'BOLETO']);
        Pagamento::create(['forma_pagamento' => 'CRÉDITO À VISTA']);
        Pagamento::create(['forma_pagamento' => 'CRÉDITO 1X']);
        Pagamento::create(['forma_pagamento' => 'CRÉDITO 2X']);
        Pagamento::create(['forma_pagamento' => 'CRÉDITO 3X']);
        Pagamento::create(['forma_pagamento' => 'CRÉDITO 4X']);
        Pagamento::create(['forma_pagamento' => 'CRÉDITO 5X']);
        Pagamento::create(['forma_pagamento' => 'CRÉDITO 6X']);
        Pagamento::create(['forma_pagamento' => 'CRÉDITO 7X']);
        Pagamento::create(['forma_pagamento' => 'CRÉDITO 8X']);
        Pagamento::create(['forma_pagamento' => 'CRÉDITO 9X']);
        Pagamento::create(['forma_pagamento' => 'CRÉDITO 10X']);
        Pagamento::create(['forma_pagamento' => 'CRÉDITO 11X']);
        Pagamento::create(['forma_pagamento' => 'CRÉDITO 12X']);
    }
}
