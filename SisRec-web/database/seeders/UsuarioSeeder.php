<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;


class UsuarioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     * 
     * @return void
     */
    public function run()
    {
        User::create(['name' => 'Bruno','permissao'=>'admin', 'email' => 'teste@teste','password' => bcrypt('123456789'), 'cpf' => '12345678922', 'cep' => '99150000', 'logradouro'=> 'rua qualquer', 'numero' => '123' ]);
        User::create(['name' => 'Joao','permissao'=>'admin', 'email' => 'teste1@teste','password' => bcrypt('123456789'), 'cpf' => '12345678922', 'cep' => '99150000', 'logradouro'=> 'rua qualquer', 'numero' => '123']);
        User::create(['name' => 'Pedro', 'permissao'=>'admin', 'email' => 'teste2@teste','password' => bcrypt('123456789'), 'cpf' => '12345678922', 'cep' => '99150000', 'logradouro'=> 'rua qualquer', 'numero' => '123']);
        User::create(['name' => 'admin', 'permissao'=>'cliente','email' => 'cleinte@cliente','password' => bcrypt('123456789'), 'cpf' => '12345678922', 'cep' => '99150000', 'logradouro'=> 'rua qualquer', 'numero' => '123']);

    }
}