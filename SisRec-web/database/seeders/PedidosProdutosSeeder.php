<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\PedidosProdutos;
class PedidosProdutosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PedidosProdutos::create(['pedido_id' => '1', 'produto_id' => '2']);
        PedidosProdutos::create(['pedido_id' => '1', 'produto_id' => '1']);
        PedidosProdutos::create(['pedido_id' => '1', 'produto_id' => '3']);
        PedidosProdutos::create(['pedido_id' => '1', 'produto_id' => '4']);

        PedidosProdutos::create(['pedido_id' => '2', 'produto_id' => '2']);
        PedidosProdutos::create(['pedido_id' => '2', 'produto_id' => '1']);
        PedidosProdutos::create(['pedido_id' => '2', 'produto_id' => '3']);
        PedidosProdutos::create(['pedido_id' => '2', 'produto_id' => '4']);
        
        PedidosProdutos::create(['pedido_id' => '3', 'produto_id' => '2']);
        PedidosProdutos::create(['pedido_id' => '3', 'produto_id' => '1']);
        PedidosProdutos::create(['pedido_id' => '3', 'produto_id' => '3']);
        PedidosProdutos::create(['pedido_id' => '3', 'produto_id' => '4']);

    }
}
