<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Produto;

class ProdutoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Produto::create(['imagem' =>'','nome' => 'Teclado mk345', 'preco' => '178.90','qnt' => '1', 'categoria_id'=>'1', 'ativo'=>'S', 'imagem'=>'#']);
        Produto::create(['imagem' =>'','nome' => 'Mouse G604', 'preco' => '349.90','qnt' => '1', 'categoria_id'=>'1', 'ativo'=>'S', 'imagem'=>'#']);
        Produto::create(['imagem' =>'','nome' => 'Teclado T206', 'preco' => '59.90','qnt' => '1','categoria_id'=>'1', 'ativo'=>'S', 'imagem'=>'#']);
        Produto::create(['imagem' =>'','nome' => 'Controle P9067', 'preco' => '249.90','qnt' => '1', 'categoria_id'=>'1', 'ativo'=>'S', 'imagem'=>'#']);
        Produto::create(['imagem' =>'','nome' => 'HeadSet G902', 'preco' => '459.90','qnt' => '1', 'categoria_id'=>'1', 'ativo'=>'S', 'imagem'=>'#']);
        Produto::create(['imagem' =>'','nome' => 'Fone de ouvido', 'preco' => '34.90','qnt' => '1', 'categoria_id'=>'1', 'ativo'=>'S', 'imagem'=>'#']);
        Produto::create(['imagem' =>'','nome' => 'Microfne MT-902', 'preco' => '49.90','qnt' => '1', 'categoria_id'=>'1', 'ativo'=>'S', 'imagem'=>'#']);
        Produto::create(['imagem' =>'','nome' => 'Kit gamer completo', 'preco' => '399.90','qnt' => '1', 'categoria_id'=>'1', 'ativo'=>'S', 'imagem'=>'#']);
        Produto::create(['imagem' =>'','nome' => 'Volante multilaser', 'preco' => '499.90','qnt' => '1', 'categoria_id'=>'1', 'ativo'=>'S', 'imagem'=>'#']);
        Produto::create(['imagem' =>'','nome' => 'Mouse pad', 'preco' => '17.90','qnt' => '1', 'categoria_id'=>'1', 'ativo'=>'S', 'imagem'=>'#']);


    }
}
