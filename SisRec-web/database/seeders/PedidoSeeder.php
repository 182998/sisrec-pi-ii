<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Pedidos;

class PedidoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Pedidos::create(['user_id' => '1', 'pagamento_id' => '2']);
        Pedidos::create(['user_id' => '3', 'pagamento_id' => '3']);
        Pedidos::create(['user_id' => '2', 'pagamento_id' => '1']);
    }
}
