<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Produto extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('produto', function (Blueprint $table) {
            $table->id();
            $table->string('nome',255);
            $table->double('preco',8,2)->deafault(0);
            $table->string('imagem');
            $table->enum('ativo',['S','N'])->default('S');
            $table->bigInteger('qnt');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('produto');
        

        /**
        *Schema::table('categoria', function(Blueprint $table){
        *$table->foreignId('categoria_id')
        *->constrainde()
        *->onDelete('cascade');
        *});
        */
    }
}
