<?php

namespace Tests\Browser;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class RegisterProdutosTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */

    public function test_check_if_login_function_is_working()
    {
        $this->browse(function(Browser $browser){
            $browser->visit('/login')
                ->type('email','teste@teste123')
                ->type('password', '123456789')
                ->press('Login')
                ->assertPathIs('/home');
        });
    }
    
    public function test_check_if_root_side_is_correct()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/produtos')
                    ->assertSee('Lista');
        });
    }

    public function teste_check_if_produtos_function_is_working()
    {
        $this->browse(function(Browser $browser){
            $browser->visit('/produtos/create')
                ->type('nome','Produto teste')
                ->type('preco', '19.9')
                ->type('qnt', '5')
                ->press('Criar')
                ->assertPathIs('/produtos');
        });
    }

    public function teste_check_if_produtos_edit_is_working()
    {
        $this->browse(function(Browser $browser){
            $browser->visit('/produtos/4/edit')
                ->type('nome','Teste teste')
                ->type('preco', '99999.9')
                ->type('qnt', '99999')
                ->press('Editar Produto')
                ->assertPathIs('/produtos');
        });
    }

    public function teste_check_if_produtos_delete_is_working()
    {
        $this->browse(function(Browser $browser){
            $browser->visit('/produtos/7/destroy')
                ->assertPathIs('/produtos');
        });
    }
}
