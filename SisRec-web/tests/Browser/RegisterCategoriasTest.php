<?php

namespace Tests\Browser;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class RegisterCategoriasTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    
    public function test_check_if_login_function_is_working()
    {
        $this->browse(function(Browser $browser){
            $browser->visit('/login')
                ->type('email','teste@teste123')
                ->type('password', '123456789')
                ->press('Login')
                ->assertPathIs('/home');
        });
    }

    public function test_if_rout_is_correct()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/categorias')
                    ->assertSee('Categoria');
        });
    }



    public function teste_check_if_create_categoria_is_working()
    {
        $this->browse(function(Browser $browser){
            $browser->visit('/categorias/create')
                ->type('descricao', 'Teclados')
                ->press('Criar')
                ->assertPathIs('/categorias')
                ->assertSee('Categorias');
        });
    }

    public function teste_check_if_forma_categorias_edit_is_working()
    {
        $this->browse(function(Browser $browser){
            $browser->visit('/categorias/4/edit')
            ->type('descricao', 'Teste Teste')
            ->press('Editar Categoria')
            ->assertPathIs('/categorias');
        });
    }

    public function teste_check_if_categorias_delete_is_working()
    {
        $this->browse(function(Browser $browser){
            $browser->visit('/categorias/7/destroy')
                ->assertPathIs('/categorias');
        });
    }

}
