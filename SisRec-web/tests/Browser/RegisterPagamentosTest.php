<?php

namespace Tests\Browser;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class RegisterPagamentosTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */

    public function test_check_if_login_function_is_working()
    {
        $this->browse(function(Browser $browser){
            $browser->visit('/login')
                ->type('email','teste@teste123')
                ->type('password', '123456789')
                ->press('Login')
                ->assertPathIs('/home');
        });
    }
    
    public function test_if_rout_is_correct()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/pagamentos')
                    ->assertSee('Pagamento');
        });
    }


    public function teste_check_if_create_pagamentos_is_working()
    {
        $this->browse(function(Browser $browser){
            $browser->visit('/pagamentos/create')
                ->type('forma_pagamento', 'Credito')
                ->press('Criar')
                ->assertPathIs('/pagamentos')
                ->assertSee('Formas de Pagamento');
        });
    }

    
    public function teste_check_if_forma_pagamento_edit_is_working()
    {
        $this->browse(function(Browser $browser){
            $browser->visit('/pagamentos/4/edit')
                ->type('forma_pagamento','Testteste')
                ->press('Editar Pagamento')
                ->assertPathIs('/pagamentos');
        });
    }
    
    public function teste_check_if_pagamentos_delete_is_working()
    {
        $this->browse(function(Browser $browser){
            $browser->visit('/pagamentos/7/destroy')
                ->assertPathIs('/pagamentos');
        });
    }

}
