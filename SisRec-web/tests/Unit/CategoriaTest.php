<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Models\Categoria;
class CategoriaTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_example()
    {
        $categoria = new Categoria;
        $expected = [
            'descricao'
        ];
        $arrayCompared = array_diff($expected, $categoria->getFillable());
        $this->assertEquals(0,count($arrayCompared));
    }
}
