<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Models\Produto;

class ProdutosTest extends TestCase
{

    public function test_check_if_produto_columns_is_currect()
    {
        $produto = new Produto;
        $expected = [
            'nome',
            'preco',
            'qnt',
            'categoria_id'
        ];
        $arrayCompared = array_diff($expected, $produto->getFillable());
        $this->assertEquals(0,count($arrayCompared));
    }

}
