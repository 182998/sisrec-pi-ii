<?php

namespace Tests\Unit;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use App\Models\User;

class UserTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */

    public function test_check_if_user_columns_is_currect()
    {
        $user = new User;
        $expected = [
            'name',
            'email',
            'password',
            'cpf',
            'cep',
            'logradouro',
            'numero'
        ];
        $arrayCompared = array_diff($expected, $user->getFillable());
        $this->assertEquals(0,count($arrayCompared));
    }

    public function test_if_user_is_duplicated()
    {
        $user1 = User::make([
            'name' => 'Johnny',
            'email' => 'johnny@teste'
        ]);

        $user2 = User::make([
            'name' => 'Johnny',
            'email' => 'Amarildo@teste'
        ]);

        $this->assertFalse($user1->name != $user2->name);
    }
    /** COMANDO DELETA USUARIO DA TABELA
    * 
    * 
    *public function test_delete_user()
    *{
    *    $user = User::factory()->count(1)->make();
    *    
    *   $user = User::first();
    *
    *   if($user){
    *        $user->delete();
    *    }
    *    $this->assertTrue(true);
    *}
    */

    public function test_if_database_is_currect()
    {
        $this->assertDatabaseHas('users',[
            'name'=>'admin2'
        ]);
        $this->assertDatabaseHas('users',[
            'name'=>'admin'
        ]);
        $this->assertDatabaseHas('users',[
            'name'=>'Bruno'
        ]);
        $this->assertDatabaseHas('users',[
            'name'=>'Bruno2'
        ]);
        
    }
    /*
    public function teste_if_seeder_works()
    {
        $this->seed();
    }
    */

}
