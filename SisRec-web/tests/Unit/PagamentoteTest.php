<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Models\Pagamento;

class PagamentoteTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_example()
    {
        $pagamento = new Pagamento;
        $expected = [
            'forma_pagamento'
        ];
        $arrayCompared = array_diff($expected, $pagamento->getFillable());
        $this->assertEquals(0,count($arrayCompared));
    }
}
